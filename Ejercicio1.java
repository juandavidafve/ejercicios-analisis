public class Ejercicio1 {

    /*
     * En un array de n elementos se almacenan los números de 1 a n+1, excepto uno
     * de ellos. Encuentre cuál falta en un orden como máximo de O(n).
     *
     */
    public static int findMissing(int[] a) {
        boolean[] a2 = new boolean[a.length + 2];
        int found = -1;

        for (int i = 0; i < a.length; i++) {
            int elem = a[i];
            a2[elem] = true;
        }

        for (int i = 1; i < a2.length; i++) {
            boolean elem = a2[i];

            if (!elem) {
                found = i;
            }
        }

        return found;
    }

}