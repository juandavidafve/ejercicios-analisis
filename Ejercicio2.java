public class Ejercicio2 {

	/*
	 * Se dice que un número n es triangular se puede expresar como la suma de los j
	 * primeros números. Determinar si un número n es triangular o no.
	 */
	public static boolean isTriangular(int n) {
		boolean isTriangular = false;
		int i = 1;
		int sum = 0;

		while (sum < n) {
			sum += i;
			i++;
		}

		if (sum == n) {
			isTriangular = true;
		}

		return isTriangular;
	}

}